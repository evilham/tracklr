.. _calendarsetups:

Calendar Setups
---------------

You can use any available web application which provides you with icalendar (``.ics``) feed eg.
Google Calendar, Apple iCloud Calendar, Microsoft Outlook/Office 365 Calendar, etc.

Firstly though, you need to decide how you want to work with your calendars.

Here are few possible options for personal and business use.

1) One calendar to rule them all

   Pros:

   * simple
   * all ``@customer`` ``+actions`` done by ``!users`` ``#in-one-place``

   Cons:

   * not great for multi users usage

-----

2) Two calendars: ``todo`` and ``done``

   Pros:

   * simple workflow ie. plan in ``todo``, move to ``done`` when done
   * clear reporting ie. it is either ``done`` or it is ``todo``

   Cons:

   * not great for multi users usage


-----

3) One calendar per ``@customer``

   Pros:

   * good for billing

   Cons:

   * not great for multi users usage

-----

4) One calendar per ``@customer``/``#project`` per ``!user``

   Pros:

   * clear who did what for whom
   * good multi user setup

   Cons:

   * complex
