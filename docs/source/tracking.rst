.. _tracking:

Tracking
========

The core idea behind ``Tracklr`` is to use
`iCalendar <https://icalendar.org/>`_ feeds for time tracking and focus management.

To be able to filter calendar events into meaningful reports one should find their way to
provide meaning to data.

Here are few examples of tracking styles.

The following is ``Objectives and Key Results`` style of how you can label your calendar entries:

* ``@KeepLearnin`` - use ``@`` to identify ``objectives``.
* ``#Read12Books`` - use ``#`` to identify ``key results``.
* ``+read a book`` - use ``+`` to define ``actions`` that achieve a key result.
* ``*read a book`` - use ``*`` to mark ``actions`` as done so that you can measure progress.

For business reporting you can use the following style:

* ``@Customer_Ltd`` - use ``@`` to identify ``customers``.
* ``#INITIATIVE_X`` - use ``#`` to identify ``initiatives`` done for a customer.

For use cases when one calendar is shared by multiple users:

* ``!JB`` - use ``!`` to identify ``who`` a calendar entry relates to.

Also see :ref:`philosophy`, :ref:`usage` and :ref:`calendarsetups` information.
