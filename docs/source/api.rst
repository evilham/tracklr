API Reference
=============

Sample code to illustrate ``Tracklr`` code functionality using a demo `iCalendar` feed.

.. code-block:: shell

    >>> from tracklr import Tracklr
    >>> t = Tracklr()
    >>> t.__version__
    '1.5.2'
    >>> report = t.get_report(None, None, None, None, None)
    >>> t.total_hours
    47.0
    >>> for event in report:
    ...     print("{} | {} | {}".format(event[0], event[1], event[3]))
    ...     
    ... 
    2019-02-09 | @Tracklr #v0.1.0 | 4.0
    2019-02-15 | @Tracklr #v0.1.0 | 0.5
    2019-02-15 | @Tracklr #v0.2.0 | 2.0
    2019-02-19 | @Tracklr #v0.2.0 | 1.0
    2019-02-20 | @Tracklr #v0.2.0 | 0.5
    2019-02-21 | @Tracklr #v0.3.0 | 1.0
    2019-02-22 | @Tracklr #v0.3.0 | 1.0
    2019-02-22 | @Tracklr #v0.4.0 | 2.0
    2019-02-23 | @Tracklr #v0.5.0 | 2.0
    2019-03-01 | @Tracklr #v0.6.0 | 1.0
    2019-03-01 | @Tracklr #v0.6.0 | 1.0
    2019-03-29 - 2019-03-30 | @Tracklr #v0.7.0 | 6.0
    2019-05-03 | @Tracklr #v0.8.0 | 2.0
    2019-06-04 | @Tracklr #v0.9.0 | 1.0
    2019-06-10 | @Tracklr #v0.9.1 | 0.5
    2019-06-10 | @Tracklr #v0.9.2 | 1.0
    2019-07-26 | @Tracklr #v0.9.3 | 0.5
    2019-08-16 | @Tracklr #v0.9.3 | 2.0
    2019-11-20 | @Tracklr #v0.9.4 | 1.0
    2019-12-24 | @Tracklr #v0.9.5 | 2.0
    2019-12-25 | @Tracklr #v1.0.0 | 2.0
    2019-12-26 | @Tracklr #v1.0.0 | 2.0
    2019-12-30 | @Tracklr #v1.0.1 | 2.0
    2019-12-31 | @Tracklr #v1.1.0 | 2.0
    2020-01-01 | @Tracklr #v1.1.1 | 2.0
    2020-01-01 | @Tracklr #v1.1.2 | 1.0
    2020-01-04 | @Tracklr #v1.1.3 | 2.0
    2020-01-04 | @Tracklr #v1.2.0 | 2.0
    >>> 


Version ``v0.7.0`` event does not look correct, because it spans across two days and it should not, because the event is set to `2019-03-30` in the calendar.

`Tracklr Demo` is a Google Calendar feed which uses ``X-WR-TIMEZONE`` to store the timezone information so to get a correct report, ``timezone`` needs to be set to ``True``:

.. code-block:: shell

    >>> t.calendars['default']['timezone']
    Traceback (most recent call last):
      File "<input>", line 1, in <module>
          t.calendars['default']['timezone']
          KeyError: 'timezone'
          >>> t.calendars['default']['timezone'] = True
          >>> report = t.get_report(None, None, None, None)
          >>> for event in report:
          ...     print("{} | {} | {}".format(event[0], event[1], event[3]))
    ...     
    ... 
    2019-02-09 | @Tracklr #v0.1.0 | 4.0
    2019-02-16 | @Tracklr #v0.1.0 | 0.5
    2019-02-16 | @Tracklr #v0.2.0 | 2.0
    2019-02-20 | @Tracklr #v0.2.0 | 1.0
    2019-02-21 | @Tracklr #v0.2.0 | 0.5
    2019-02-22 | @Tracklr #v0.3.0 | 1.0
    2019-02-22 | @Tracklr #v0.3.0 | 1.0
    2019-02-23 | @Tracklr #v0.4.0 | 2.0
    2019-02-24 | @Tracklr #v0.5.0 | 2.0
    2019-03-01 | @Tracklr #v0.6.0 | 1.0
    2019-03-02 | @Tracklr #v0.6.0 | 1.0
    2019-03-30 | @Tracklr #v0.7.0 | 6.0
    2019-05-04 | @Tracklr #v0.8.0 | 2.0
    2019-06-04 | @Tracklr #v0.9.0 | 1.0
    2019-06-10 | @Tracklr #v0.9.1 | 0.5
    2019-06-10 | @Tracklr #v0.9.2 | 1.0
    2019-07-27 | @Tracklr #v0.9.3 | 0.5
    2019-08-17 | @Tracklr #v0.9.3 | 2.0
    2019-11-20 | @Tracklr #v0.9.4 | 1.0
    2019-12-24 | @Tracklr #v0.9.5 | 2.0
    2019-12-25 | @Tracklr #v1.0.0 | 2.0
    2019-12-27 | @Tracklr #v1.0.0 | 2.0
    2019-12-30 | @Tracklr #v1.0.1 | 2.0
    2019-12-31 | @Tracklr #v1.1.0 | 2.0
    2020-01-01 | @Tracklr #v1.1.1 | 2.0
    2020-01-01 | @Tracklr #v1.1.2 | 1.0
    2020-01-04 | @Tracklr #v1.1.3 | 2.0
    2020-01-05 | @Tracklr #v1.2.0 | 2.0
    >>> 


Alternatively, ``timezone`` can be explicitly set as well:

.. code-block:: shell

    >>> t.calendars['default']['timezone'] = 'Pacific/Auckland'
    >>> report = t.get_report(None, None, None, None)
    >>> for event in report:
    ...     print("{} | {} | {}".format(event[0], event[1], event[3]))
    ...     
    ... 
    2019-02-09 | @Tracklr #v0.1.0 | 4.0
    2019-02-16 | @Tracklr #v0.1.0 | 0.5
    2019-02-16 | @Tracklr #v0.2.0 | 2.0
    2019-02-20 | @Tracklr #v0.2.0 | 1.0
    2019-02-21 | @Tracklr #v0.2.0 | 0.5
    2019-02-22 | @Tracklr #v0.3.0 | 1.0
    2019-02-22 | @Tracklr #v0.3.0 | 1.0
    2019-02-23 | @Tracklr #v0.4.0 | 2.0
    2019-02-24 | @Tracklr #v0.5.0 | 2.0
    2019-03-01 | @Tracklr #v0.6.0 | 1.0
    2019-03-02 | @Tracklr #v0.6.0 | 1.0
    2019-03-30 | @Tracklr #v0.7.0 | 6.0
    2019-05-04 | @Tracklr #v0.8.0 | 2.0
    2019-06-04 | @Tracklr #v0.9.0 | 1.0
    2019-06-10 | @Tracklr #v0.9.1 | 0.5
    2019-06-10 | @Tracklr #v0.9.2 | 1.0
    2019-07-27 | @Tracklr #v0.9.3 | 0.5
    2019-08-17 | @Tracklr #v0.9.3 | 2.0
    2019-11-20 | @Tracklr #v0.9.4 | 1.0
    2019-12-24 | @Tracklr #v0.9.5 | 2.0
    2019-12-25 | @Tracklr #v1.0.0 | 2.0
    2019-12-27 | @Tracklr #v1.0.0 | 2.0
    2019-12-30 | @Tracklr #v1.0.1 | 2.0
    2019-12-31 | @Tracklr #v1.1.0 | 2.0
    2020-01-01 | @Tracklr #v1.1.1 | 2.0
    2020-01-01 | @Tracklr #v1.1.2 | 1.0
    2020-01-04 | @Tracklr #v1.1.3 | 2.0
    2020-01-05 | @Tracklr #v1.2.0 | 2.0
    >>> 


Tracklr
-------

.. automodule:: tracklr
      :members:

init
----

.. automodule:: tracklr.init
      :members:

info
----

.. automodule:: tracklr.info
      :members:

ls
--

.. automodule:: tracklr.ls
      :members:

group
-----

.. automodule:: tracklr.group
      :members:

pdf
---

.. automodule:: tracklr.pdf
      :members:
