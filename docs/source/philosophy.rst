.. _philosophy:

Philosophy
==========

``Tracklr`` helps you to implement *write down the solution* from
`The Feynman Algorithm <http://wiki.c2.com/?FeynmanAlgorithm>`_.


Use any online calendar service which provides you with
`iCalendar <https://icalendar.org/>`_ feed to execute the following
flow to achieve your goals:

1. Plan out what and when you want to get done in your calendar
2. Do it as you planned out
3. Report what you got done using ``Tracklr``

There are many ways how to set yourself up with an online
calendar which provides ``iCalendar`` feeds. The choice is yours.

No matter what your calendar setup is the process remains
the same:

1. Add events with your planned items into your calendar
2. Do what you planned when the time comes
3. Use ``Tracklr`` to report what you have achieved

Also see :ref:`usage`, :ref:`tracking` and :ref:`calendarsetups` information.
